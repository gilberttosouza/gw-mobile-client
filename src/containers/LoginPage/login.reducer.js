import * as types from './login.types'
import { conversations } from '../../constants/initialState'

export default (state = conversations, action) => {
  switch (action.type) {
    case types.ADD_CONVERSATION:
      return Object.assign({}, action.payload)
    default:
      return state
  }
}
