import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'

import * as loginCreators from './login.actions'

import LoginForm from '../../components/login/LoginForm'
import Logo from '../../images/logo.png'

const styles = theme => ({
  root: {
    marginTop: 100,
    backgroundColor: '#f9f9f9',
    padding: theme.spacing.unit,
  },
  logo: {
    maxWidth: '100%',
    display: 'block',
  },
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    height: '100%',
  },
  flexCenter: {
    alignItems: 'center',
    display: 'flex',
    textAlign: 'center',
    justifyContent: 'center',
  },
})

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      login: {
        email: '',
        password: '',
      },
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleInputChange(event) {
    const field = event.target.name
    const { login } = this.state
    login[field] = event.target.value
    this.setState({ login })
  }

  handleSubmit(event) {
    event.preventDefault()
    const { login } = this.state
    console.log(login)
  }

  render() {
    const { login } = this.state
    const { classes } = this.props
    return (
      <Grid container className={classes.root}>
        <Grid item xs={12}>
          <Grid
            container
            spacing={16}
            justify="center"
            alignItems="center"
            direction="row"
          >
            <Paper className={classes.paper} elevation={1}>
              <div className={classes.flexCenter}>
                <img src={Logo} className={classes.logo} alt="Pontti" />
              </div>
              <LoginForm
                email={login.email}
                password={login.password}
                handleSubmit={this.handleSubmit}
                handleInputChange={this.handleInputChange}
              />
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

const mapStateToProps = state => ({
  messages: state.login.isAuthenticated,
})

const mapDispatchToProps = dispatch => bindActionCreators(loginCreators, dispatch)

const LoginStyles = withStyles(styles)(Login)

const LoginConnect = connect(mapStateToProps, mapDispatchToProps)(LoginStyles)

export default withRouter(LoginConnect)
