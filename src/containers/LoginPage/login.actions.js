import * as types from './login.types'

const addConversationSuccess = data => ({
  type: types.ADD_CONVERSATION,
  payload: data,
})

export const addConversation = userConversation => (dispatch) => {
  return new Promise((resolve) => {
    dispatch(addConversationSuccess(userConversation))
    resolve()
  })
}
