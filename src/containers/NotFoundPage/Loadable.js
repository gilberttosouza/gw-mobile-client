import Loadable from 'react-loadable'

import LoadinIndicatior from '../../components/common/LoadingIndicator'

export default Loadable({
  loader: () => import('./index'),
  loading: LoadinIndicatior,
})
