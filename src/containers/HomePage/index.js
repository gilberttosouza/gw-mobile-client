import React from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import { withStyles } from '@material-ui/core/styles'

import Logo from '../../images/logo.png'

const styles = theme => ({
  root: {
    height: '100%',
  },
  content: {
    height: '100%',
    padding: theme.spacing.unit,
  },
})

const Home = ({ classes }) => (
  <section className={classes.root}>
    <div className={classes.content}>
      <img src={Logo} alt="Pontti" />
    </div>
    <AppBar position="static">
      <Toolbar />
    </AppBar>
  </section>
)

Home.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

const HomeStyled = withStyles(styles)(Home)

export default HomeStyled
