import React from 'react'
import { Helmet } from 'react-helmet'
import CssBaseline from '@material-ui/core/CssBaseline'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import PrivateRoute from '../HighOrderComponents/PrivateRoute'
import Login from './LoginPage/Loadable'
import HomePage from './HomePage/Loadable'
import NotFoundPage from './NotFoundPage/Loadable'

const App = () => (
  <React.Fragment>
    <CssBaseline />
    <Helmet
      titleTemplate="%s Pontti soluções"
      defaultTitle="App Pontti"
    >
      <meta name="description" content="A app Pontti application" />
    </Helmet>
    <Router>
      <TransitionGroup>
        <CSSTransition classNames="fade" timeout={30}>
          <Switch>
            <PrivateRoute exact path="/" component={HomePage} />
            <Route path="/login" component={Login} />
            <Route path="" component={NotFoundPage} />
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    </Router>
  </React.Fragment>
)

export default App
