// Fake response
const conversations = [
  {
    id: 1,
    texts: [
      'Text contact',
      'Text contact message',
    ],
    self: false,
    avatar: 'https://img.itdg.com.br/tdg/images/users_avatars/002/352/118/222173/222173_original.jpg?mode=crop&width=60&height=60',
  },
  {
    id: 2,
    texts: ['Text my Message'],
    self: true,
    avatar: 'https://img.itdg.com.br/tdg/images/users_avatars/002/352/118/222173/222173_original.jpg?mode=crop&width=60&height=60',
  },
]

class chatService {
  static getConversations() {
    return conversations
  }

  static setMessage(id, texts) {
    const indexConversation = conversations.findIndex((e => e.id === id))
    if (indexConversation !== -1) {
      conversations[indexConversation].texts.push(texts)
    }
  }
}

export default chatService
