import React from 'react'
import Typography from '@material-ui/core/Typography'

const LoadingIndicator = () => (
  <Typography>
    Loading...
  </Typography>
)

export default LoadingIndicator
