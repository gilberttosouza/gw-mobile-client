import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  button: {
    marginTop: theme.spacing.unit,
  },
})

const LoginForm = ({
  email,
  password,
  handleInputChange,
  handleSubmit,
  classes,
}) => (
  <form onSubmit={handleSubmit}>
    <TextField
      id="email"
      name="email"
      label="E-mail"
      value={email}
      fullWidth
      margin="normal"
      onChange={handleInputChange}
    />
    <TextField
      id="password"
      name="password"
      label="Senha"
      type="password"
      value={password}
      fullWidth
      margin="normal"
      onChange={handleInputChange}
    />
    <Button
      type="submit"
      variant="contained"
      color="primary"
      className={classes.button}
      fullWidth
    >
      Login
    </Button>
  </form>
)


LoginForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
}

const LoginFormStyles = withStyles(styles)(LoginForm)

export default LoginFormStyles
