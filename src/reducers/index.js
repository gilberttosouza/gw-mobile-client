import { combineReducers } from 'redux'
import login from '../containers/LoginPage/login.reducer'

const rootReducers = combineReducers({
  login,
})

export default rootReducers
