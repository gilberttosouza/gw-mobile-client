# Project client develope with React
Coming soon description

### Software develop by Pontti
![Print chat](./public/images/logo.png)

### Libraries used
* [React]
* [Redux]
* [Redux-Thunk]
* [React-Fontawesome]
* [React-Loadable]
* [Material-ui for React]
* [Font-Awesome]
* [Prop-Types]
* [Enzyme]
* [Enzyme-adapter-react-16]
* [Enzyme-to-json]
* [Jest]

### Directory structure
Coming soon description


### Installation
Clone this repository and install dependency with `yarn install` or `npm install`
```sh
$ cd gw-mobile-client
$ yarn install
$ yarn start or npm start
```
### Run Test

```sh
$ yarn test
```




